const process = require('child_process')
const util = require('util')
const exec = util.promisify(process.exec)

describe('caniuse', () => {
  it('version', async () => {
    const { stdout } = await exec('npm ls caniuse-lite')
    const result = stdout.match(/caniuse-lite@[\d\.]+/g)
    expect(result.length).toBeGreaterThanOrEqual(1)
    for (let i = 1; i < result.length; i++) {
      const item = result[i]
      expect(item).toBe(result[0])
    }
  })
})
