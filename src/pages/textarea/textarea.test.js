describe('textarea', () => {
  let page
  beforeAll(async () => {
    page = await program.reLaunch('/pages/textarea/textarea')
    await page.waitFor(3000)
  })
  it('auto-height', async () => {
    const LINE_HEIGHT = 16
    const textarea = await page.$('.textarea')
    const { height } = await textarea.size()
    expect(height).toBeGreaterThan(LINE_HEIGHT);
    {
      await page.setData({
        value: '\n'
      })
      await page.waitFor(1000)
      const { height } = await textarea.size()
      expect(height).toBeGreaterThan(LINE_HEIGHT * 2)
    }
    {
      await page.setData({
        value: '\n\n'
      })
      await page.waitFor(1000)
      const { height } = await textarea.size()
      expect(height).toBeGreaterThan(LINE_HEIGHT * 3)
    }
    page = await program.navigateTo('/pages/textarea/new-page')
    await page.waitFor(3000)
    page = await program.navigateBack()
    await page.waitFor(3000);
    {
      await page.setData({
        value: '\n\n\n'
      })
      await page.waitFor(1000)
      const { height } = await textarea.size()
      expect(height).toBeGreaterThan(LINE_HEIGHT * 4)
    }
  })
})