describe('if', () => {
  let page
  beforeAll(async () => {
    page = await program.reLaunch('/pages/if/if')
    await page.waitFor(3000)
  })
  it('init', async () => {
    const index = await page.callMethod('getIndex')
    expect(index).toBe(4)
  })
  it('test1 = true', async () => {
    await page.setData({
      test1: true
    })
    const index = await page.callMethod('getIndex')
    expect(index).toBe(12)
  })
  it('test2 = true', async () => {
    await page.setData({
      test2: true
    })
    const index = await page.callMethod('getIndex')
    expect(index).toBe(22)
  })
})