describe('easycom', () => {
  let page
  beforeAll(async () => {
    page = await program.reLaunch('/pages/easycom/easycom')
    await page.waitFor(3000)
  })
  it('import from easycom', async () => {
    const list = ['test-easy-com', 'com-test', 'easy-test', 'easy-com-test', 'easy-com-com']
    expect.assertions(list.length)
    for (let i = 0; i < list.length; i++) {
      const id = list[i]
      const element = await page.$(`#${id}`)
      const text = await element.text()
      expect(text).toBe(id)
    }
  })
  it('import from local', async () => {
    const list = ['local-test1', 'local-test2']
    expect.assertions(list.length)
    for (let i = 0; i < list.length; i++) {
      const id = list[i]
      const element = await page.$(`#${id}`)
      const text = await element.text()
      expect(text).toBe('local-component')
    }
  })
})