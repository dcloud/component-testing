describe('wxs', () => {
  let page
  beforeAll(async () => {
    page = await program.reLaunch('/pages/wxs/wxs')
    await page.waitFor(3000)
  })
  it('wxs', async () => {
    expect.assertions(3)
    const elements = await page.$$('.wxs')
    const array = await page.data('array')
    for (let index = 0; index < array.length; index++) {
      const item = array[index]
      const text = await elements[index].text()
      expect(text).toBe(item)
    }
  })
})
