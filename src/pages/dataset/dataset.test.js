describe('dataset', () => {
    let page
    beforeAll(async () => {
        page = await program.reLaunch('/pages/dataset/dataset')
        await page.waitFor(3000)
    })
    const types = ['string', 'number', 'boolean', 'object']
    it('event', async () => {
        expect.assertions(types.length)
        for(let index = 0; index < types.length; index++) {
            const type = types[index]
            const element = await page.$('#' + type)
            await element.tap()
            const dataset = await page.data('dataset')
            const value = await page.data(type)
            expect(dataset.testText).toEqual(value)
        }
    })
    it('boundingClientRect', async () => {
        expect.assertions(types.length)
        for(let index = 0; index < types.length; index++) {
            const type = types[index]
            const dataset = await page.callMethod('getBoundingClientRect', type)
            const value = await page.data(type)
            expect(dataset.testText).toEqual(value)
        }
    })
})