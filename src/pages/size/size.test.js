describe('size', () => {
  let page
  let systemInfo
  beforeAll(async () => {
    page = await program.reLaunch('/pages/size/size')
    await page.waitFor(3000)
    systemInfo = await program.systemInfo()
  })
  it('rpx', async () => {
    const { windowWidth } = systemInfo
    expect.assertions(1)
    const element = await page.$('.w750rpx')
    const size = await element.size()
    const diff = Math.abs(size.width - windowWidth)
    // 安卓设备差异1px
    expect(diff).toBeLessThan(2)
  })
  it('vw', async () => {
    const { windowWidth } = systemInfo
    expect.assertions(1)
    const element = await page.$('.w100vw')
    const size = await element.size()
    const diff = Math.abs(size.width - windowWidth)
    // 安卓设备差异1px
    expect(diff).toBeLessThan(2)
  })
  it('em', async () => {
    expect.assertions(1)
    const element = await page.$('.w20em')
    const size = await element.size()
    expect(size.width).toBe(20 * 16)
  })
  it('rem', async () => {
    expect.assertions(1)
    const element = await page.$('.w20rem')
    const size = await element.size()
    expect(size.width).toBe(20 * 16)
  })
})
