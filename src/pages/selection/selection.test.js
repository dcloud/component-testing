describe('selection', () => {
  let page
  beforeAll(async () => {
    page = await program.reLaunch('/pages/selection/selection')
    await page.waitFor(3000)
  })
  // 微信小程序模拟器不支持 getSelectedTextRange
  const isWX = process.env.UNI_PLATFORM === 'mp-weixin'
  it('getSelectedTextRange', async () => {
    await page.setData({
      value: 'test',
      focus: 'input'
    })
    await page.waitFor(1000)
    await page.callMethod('getSelectedTextRange')
    let start = await page.data('start')
    expect(start).toBe(isWX ? -1 : 4)
    let end = await page.data('end')
    expect(start).toBe(isWX ? -1 : 4)
    await page.setData({
      focus: 'none',
      start: -1,
      end: -1
    })
    await page.waitFor(1000)
    await page.setData({
      focus: 'textarea'
    })
    await page.waitFor(1000)
    await page.callMethod('getSelectedTextRange')
    start = await page.data('start')
    expect(start).toBe(isWX ? -1 : 4)
    end = await page.data('end')
    expect(start).toBe(isWX ? -1 : 4)
  })
})
