describe('request', () => {
  let page
  beforeAll(async () => {
    page = await program.reLaunch('/pages/request/request')
    await page.waitFor(3000)
  })
  it('request', async () => {
    const res = await page.callMethod('request')
    expect(res.data).toBe('test=test\n')
    expect(res.errMsg).toBe('request:ok')
    expect(res.header['content-type'] || res.header['Content-Type']).toBe('text/html; charset=UTF-8')
  })
})