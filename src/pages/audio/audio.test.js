describe('audio', () => {
  let page
  beforeAll(async () => {
    page = await program.reLaunch('/pages/audio/audio')
    await page.waitFor(3000)
  })
  it('size', async () => {
    expect.assertions(2)
    const canvas = await page.$('#audio')
    const size = await canvas.size()
    expect(size.width).toBeGreaterThan(10)
    expect(size.height).toBeGreaterThan(10)
  })
  it('context', async () => {
    const methods = ['setSrc', 'play', 'seek', 'pause']
    expect.assertions(methods.length)
    for (let i = 0; i < methods.length; i++) {
      const method = methods[i]
      const result = await page.callMethod(method)
      await page.waitFor(2000)
      expect(result).toBe('success')
    }
  })
})