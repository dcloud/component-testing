describe('for', () => {
  let page
  beforeAll(async () => {
    page = await program.reLaunch('/pages/for/for')
    await page.waitFor(3000)
  })
  it('event', async () => {
    const element = await page.$('.event-test-1')
    await element.tap()
    const index = await page.$('.click-index')
    const text = await index.text()
    expect(text).toBe('index:0')
    const result = [
      'click,undefined,undefined',
      'click,undefined,undefined',
      'click,test1,0',
      'click,test1,0',
      'custom,test1,0',
      'custom,test1,0',
      'click,test1,5',
      'click,test1,0',
      'click,test1,0',
    ]
    for (let index = 0; index < result.length; index++) {
      const test = result[index]
      const reset = await page.$('.reset-info')
      await reset.tap()
      let element = await page.$(`.event-test-${index + 2}`)
      if (test.startsWith('custom')) {
        element = await element.$('.custom-test')
      }
      await element.tap()
      await page.waitFor(100)
      const info = await page.$('.event-info')
      const text = await info.text()
      expect(text).toBe(`info:${test}`)
    }
  })
  it('view', async () => {
    const result = [
      '0:test1',
      'value:test1',
      'value:obj:test1',
      'value:test1',
      'value:obj:test1',
      '0:0:test1',
      'value:test1,test2',
      'value:test1',
      'value:obj:test1',
      'test'
    ]
    for (let index = 0; index < result.length; index++) {
      const element = await page.$(`.view-test-${index + 1}`)
      const text = await element.text()
      expect(text).toBe(result[index])
    }
  })
  it('map object', async () => {
    const result = [
      'test1:obj:test1',
      'test1:obj:test1',
      'test1:obj:test1',
    ]
    for (let index = 0; index < result.length; index++) {
      const element = await page.$(`.map-object-${index + 1}`)
      const text = await element.text()
      expect(text).toBe(result[index])
    }
  })
  it('map number', async () => {
    const result = [
      '0',
      'value:test1',
    ]
    for (let index = 0; index < result.length; index++) {
      const element = await page.$(`.map-number-${index + 1}`)
      const text = await element.text()
      expect(text).toBe(result[index])
    }
  })
  it('component', async () => {
    const result = {
      'text-test': 'text:test1',
      'value-test': 'value:value:obj:test1',
      'obj-test': 'obj.text:test1',
    }
    const keys = Object.keys(result)
    for (let index = 0; index < keys.length; index++) {
      const key = keys[index]
      let element = await page.$('.child-test')
      element = await element.$(`.${key}`)
      const text = await element.text()
      expect(text).toBe(result[key])
    }
  })
  it('array event', async () => {
    const len = 4;

    const arrayEventTestItem = await page.$$('.array-event-test-item')
    expect(arrayEventTestItem.length).toBe(len);

    for (let index = 0; index < len; index++) {
      const currentItem = arrayEventTestItem[index];
      const arrayEventTestItemInfo = await currentItem.text();

      await currentItem.tap();
      await page.waitFor(500);

      const arrayEventInfoElement = await page.$('.array-event-info');
      const arrayEventInfo = await arrayEventInfoElement.text();
      
      expect(arrayEventInfo.split(':')[1]).toBe(arrayEventTestItemInfo);
    }
  })
})