describe('show', () => {
  let page
  beforeAll(async () => {
    page = await program.reLaunch('/pages/show/show')
    await page.waitFor(3000)
  })
  it('show = false', async () => {
    await page.setData({
      show: false
    })
    const elements = await page.$$('show-test')
    for (let i = 0; i < elements.length; i++) {
      const element = elements[i]
      const height = await element.size().height
      expect(height).toBe(0)
    }
  })
  it('show = true', async () => {
    await page.setData({
      show: true
    })
    const elements = await page.$$('show-test')
    for (let i = 0; i < elements.length; i++) {
      const element = elements[i]
      const height = await element.size().height
      expect(height).toBeGreaterThan(0)
    }
  })
})