describe('image', () => {
  let page
  beforeAll(async () => {
    page = await program.reLaunch('/pages/image/image')
    await page.waitFor(3000)
  })
  it('size', async () => {
    const max = 3
    expect.assertions(max * 2)
    for (let index = 0; index < max; index++) {
      const element = await page.$(`.size${index + 1}`)
      const size = await element.size()
      expect(size.width).toBe(120)
      expect(size.height).toBe(40)
    }
  })
  it('event', async () => {
    const element = await page.$('.event')
    const loadInfoElement = await page.$('.load-info')
    const errorInfoElement = await page.$('.error-info')
    let size = await element.size()
    expect(size.height).toBe(0)
    await page.setData({
      src: '/static/logo.png'
    })
    await page.waitFor(500)
    size = await element.size()
    expect(size.height).toBe(100)
    let loadInfo = await loadInfoElement.text()
    expect(loadInfo).toBe('72,72')
    let errorInfo = await errorInfoElement.text()
    expect(errorInfo).toBe('')
    await page.setData({
      loadInfo: '',
      errorInfo: ''
    })
    await page.setData({
      src: 'error'
    })
    await page.waitFor(500)
    loadInfo = await loadInfoElement.text()
    expect(loadInfo).toBe('')
    errorInfo = await errorInfoElement.text()
    expect(errorInfo.length).toBeGreaterThan(0)
    await page.setData({
      loadInfo: '',
      errorInfo: ''
    })
    await page.setData({
      src: ''
    })
    loadInfo = await loadInfoElement.text()
    expect(loadInfo).toBe('')
    errorInfo = await errorInfoElement.text()
    expect(errorInfo).toBe('')
  })
  it('mode', async () => {
    const element = await page.$('.mode')
    let size = await element.size()
    expect(size.height).toBe(100)
    await page.setData({
      mode: 'scaleToFill'
    })
    await page.waitFor(500)
    size = await element.size()
    expect(size.height).toBe(0)
    await page.setData({
      mode: 'widthFix'
    })
    await page.waitFor(500)
    size = await element.size()
    expect(size.height).toBe(100)
  })
  it('background-image', async () => {
    const base64 = await page.data('base64')
    const background0 = `url("${base64}")`
    {
      const element = await page.$('.background1')
      const background = await element.style('background-image')
      expect(background).toBe(background0)
    }
    {
      const element = await page.$('.background2')
      const background = await element.style('background-image')
      expect(background).toBe(background0)
    }
  })
})
