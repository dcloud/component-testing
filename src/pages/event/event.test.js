describe('event', () => {
  let page
  beforeAll(async () => {
    page = await program.reLaunch('/pages/event/event')
    await page.waitFor(3000)
  })
  it('page', async () => {
    expect.assertions(2)
    const elements = await page.$$('.click-test-page')
    if (process.env.UNI_PLATFORM !== "h5" && process.env.UNI_PLATFORM !== "app-plus") {
      // 理论上小程序端需要在子组件中继续查找，但实际微信小程序端无需
    }
    for (let i = 0; i < elements.length; i++) {
      const element = elements[i]
      await element.tap()
    }
    const index = await page.data('index')
    expect(elements.length).toBe(8)
    expect(index).toBe(8)
  })
  it('child', async () => {
    expect.assertions(2)
    let childTest2
    if (process.env.UNI_PLATFORM === "h5" || process.env.UNI_PLATFORM === "app-plus") {
      childTest2 = page
    } else {
      childTest2 = await page.$('child-test2')
    }
    await page.setData({
      index: 0
    })
    const elements = await childTest2.$$('.click-test-child')
    if (process.env.UNI_PLATFORM !== "h5" && process.env.UNI_PLATFORM !== "app-plus") {
      let children = await childTest2.$$('slot-scope-test')
      for (let i = 0; i < children.length; i++) {
        let child = children[i]
        child = await child.$('scoped-slots-default')
        const array = await child.$$('.click-test-child')
        elements.push(...array)
      }
    }
    for (let i = 0; i < elements.length; i++) {
      const element = elements[i]
      await element.tap()
    }
    // H5、App 端无法获取自定义组件改为从 page 获取数据
    const index = await page.data('index')
    expect(elements.length).toBe(8)
    expect(index).toBe(8)
  })
  it('wxcomponents', async () => {
    const array = [undefined, 'test', 'test', 'test']
    for (let index = 0; index < array.length; index++) {
      let element = await page.$(`.custom-event${index + 1}`)
      const child = await element.$('view')
      element = child || element
      await page.setData({ eventDetail: '' })
      await element.tap()
      const eventDetail = await page.data('eventDetail')
      expect(eventDetail).toBe(array[index])
    }
  })
})