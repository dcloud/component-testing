describe('export', () => {
  let page
  beforeAll(async () => {
    page = await program.reLaunch('/pages/export/export')
    await page.waitFor(3000)
  })
  it('text', async () => {
    const list = ['default-test', 'default-extend-test', 'default-extend-const-test', 'const-test', 'const-extend-test']
    expect.assertions(list.length)
    for (let i = 0; i < list.length; i++) {
      const select = list[i]
      const element = await page.$(`.${select}`)
      const text = await element.text()
      expect(text).toBe(select)
    }
  })
})