const fs = require('fs')
const path = require('path')
const pixelmatch = require('pixelmatch')
const PNG = require('pngjs').PNG

/**
 * 对比当前页面 canvas 和指定图像差异
 * @param {*} page 页面对象
 * @param {*} screenshotPath 图像路径
 */
async function diff (page, screenshotPath) {
  const tempPath = path.join(__dirname, 'temp')
  await program.screenshot({
    path: path.join(tempPath, screenshotPath)
  })
  const base64 = await page.callMethod('getImageData')
  const imagePath = path.join(tempPath, screenshotPath)
  fs.writeFileSync(imagePath, Buffer.from(base64.replace('data:image/png;base64,', ''), 'base64'))
  const image = PNG.sync.read(fs.readFileSync(imagePath))
  const screenshot = PNG.sync.read(fs.readFileSync(path.join(__dirname, screenshotPath)))
  const { width, height } = image
  const diff = new PNG({ width, height })
  const value = pixelmatch(image.data, screenshot.data, diff.data, width, height, { threshold: 0.1 })
  fs.writeFileSync(path.join(tempPath, 'diff.png'), PNG.sync.write(diff))
  return value
}

describe('canvas', () => {
  let page
  // let systemInfo
  beforeAll(async () => {
    page = await program.reLaunch('/pages/canvas/canvas')
    await page.waitFor(3000)
    // systemInfo = await program.systemInfo()
    // const { statusBarHeight } = systemInfo
  })
  it('size', async () => {
    expect.assertions(1)
    const canvas = await page.$('.canvas')
    const size = await canvas.size()
    expect(size).toEqual({
      width: 300,
      height: 300
    })
  })
  it('text', async () => {
    expect.assertions(1)
    const screenshotPath = 'text.png'
    await page.callMethod('drawText')
    const value = await diff(page, screenshotPath)
    expect(value).toBeLessThan(100)
  })
  it('measureText', async () => {
    expect.assertions(1)
    const textMetrics = await page.callMethod('measureText')
    expect(textMetrics.width).toBeGreaterThan(0)
  })
})