describe('slot', () => {
  let page
  beforeAll(async () => {
    page = await program.reLaunch('/pages/slot/slot')
    await page.waitFor(3000)
  })
  it('page slot', async () => {
    const max = 3
    expect.assertions(max * 2)
    for (let i = 0; i < max; i++) {
      const select = `slot-test${i + 1}`
      const element = await page.$(`.${select}`)
      const text = await element.text()
      expect(text).toBe(select)
      const color = await element.style('color')
      expect(color).toBe('rgb(255, 0, 0)')
    }
  })
  it('page slot-default', async () => {
    const max = 3
    expect.assertions(max * 2)
    for (let i = 0; i < max; i++) {
      const select = `slot-default-test${i + 1}`
      const element = await page.$(`.${select}`)
      const text = await element.text()
      expect(text).toBe(select)
      const color = await element.style('color')
      expect(color).toBe('rgb(128, 128, 128)')
    }
  })
  it('page slot-named', async () => {
    const max = 2
    expect.assertions(max * 2)
    for (let i = 0; i < max; i++) {
      const select = `slot-named-test${i + 1}`
      const element = await page.$(`.${select}`)
      const text = await element.text()
      expect(text).toBe(select)
      const color = await element.style('color')
      expect(color).toBe('rgb(255, 165, 0)')
    }
  })
  it('page slot-scope', async () => {
    const max = 5
    expect.assertions(2 * max)
    for (let i = 0; i < max; i++) {
      const select = `slot-scope-test${i + 1}`
      let element = page
      element = await element.$(`.${select}`)
      if (!element) {
        const elements = await page.$$('slot-scope-test')
        element = elements[i]
        let child = await element.$('scoped-slots-default')
        element = child || element
        child = await element.$('custom-view')
        element = child || element
        console.log(select, element)
        element = await element.$(`.${select}`)
      }
      const text = await element.text()
      expect(text).toBe(select)
      const color = await element.style('color')
      expect(color).toBe('rgb(0, 0, 255)')
    }
  })
  it('page slot-scope-null', async () => {
    const max = 3
    expect.assertions(max * 2)
    for (let i = 0; i < max; i++) {
      const select = `slot-scope-test${i + 1}`
      const element = await page.$(`.${select}`)
      const text = await element.text()
      expect(text).toBe(select)
      const color = await element.style('color')
      expect(color).toBe('rgb(0, 0, 255)')
    }
  })
  it('component slot-scope', async () => {
    expect.assertions(2)
    let element
    if (process.env.UNI_PLATFORM === "h5" || process.env.UNI_PLATFORM === "app-plus") {
      element = page
    } else {
      element = await page.$('style-test')
      element = await element.$('slot-scope-test')
      element = await element.$('scoped-slots-default')
    }
    element = await element.$('.style-test')
    const text = await element.text()
    expect(text).toBe('style-test')
    const color = await element.style('color')
    expect(color).toBe('rgb(0, 128, 0)')
  })
})