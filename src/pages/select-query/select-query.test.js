describe('select-query', () => {
  let page
  beforeAll(async () => {
    page = await program.reLaunch('/pages/select-query/select-query')
    await page.waitFor(3000)
  })
  it('element info', async () => {
    const res = await page.callMethod('createSelectorQuery')
    expect(res).toEqual({
      dataset: {
        test: 'test'
      },
      width: 100,
      height: '40px',
      scrollLeft: 0,
      scrollTop: 0,
      scrollHeight: 0,
      scrollWidth: 0,
      margin: '0px',
      backgroundColor: 'rgb(204, 204, 204)'
    })
  })
})
